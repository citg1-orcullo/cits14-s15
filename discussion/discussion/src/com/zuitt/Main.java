//directory that contains files that is used to build an application.
//reverse domain name notation
package com.zuitt;

//"Main" class is the entry of java program and is responsible for executing
public class Main {
    public static void main(String[] args){

        // constants
        final int PRINCIPAL = 1000;
  //          PRINCIPAL = 500;

        /*
        [SECTION] Primitive Data Types
        -use to store simple values
         */

        // String quotes (char)
        char letter = 'A';
        boolean isMarried = false;
        byte students = 127;

        //-32768 to 32767
        short seats = 32767;

        // underscore may be placed in between number for code readability
        int localPopulation = 2_147_483_647;

        long worldPopulation = 7_862_081_145L;

       // [SECTION]

        float price = 12.99F;
        double temperature = 12345.6789;

        System.out.println("Result of getClass Method: ");
        System.out.println(((Object)temperature).getClass());
    }
}
